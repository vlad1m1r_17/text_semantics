# KNOWLEDGE BASE COLLECTING FROM PLAIN TEXTS IN RUSSIAN

## REQUIREMENTS
    - Neo4j 4.0.3 Enterprise Edition
    - CUDA 10.1 Toolkit (see https://www.tensorflow.org/install/gpu)
    - tensorflow 2.1.0 (pip)
    - [tensorflow-gpu 2.1.0 (pip)]
    - Keras 2.3.1 (pip)
    - stanfordnlp with Russian models
    - neomodel (need changes to work with new versions of neo4j, see https://github.com/neo4j-contrib/neomodel/pull/499)
    - another packages:  numpy, math, scipy, pickle, conllu, rusenttokenize, re, os, pathlib, time, 
        termcolor, memory_profiler, string, copy, json, collections
        
## PROJECT STRUCTURE
```
text_semantics_clear
├── dataset
│   ├── from_plain
│   │   ├── dev.txt
│   │   ├── test.txt
│   │   └── train.txt
│   ├── from_syntagrus
│   │   ├── dev.txt
│   │   ├── test.txt
│   │   └── train.txt
│   ├── kapitanskaya_dochka.txt
│   ├── plain.txt
│   ├── nature.txt
│   ├── foreigners.txt
│   └── read.txt
├── model_notebooks
│   └── create_LSTM.ipynb
├── models
│   ├── BiLSTMParser.h5
│   └── fasttext
├── python
│   ├── app
│   │   ├── example.py
│   │   └── test.py
│   ├── dataset_parsing_scripts
│   │   └── dataset_generator.py
│   ├── __init__.py
│   ├── libs
│   │   ├── API.py
│   │   ├── conllu_dependency_extractor.py
│   │   ├── constants.py
│   │   ├── db_connector.py
│   │   ├── db_extraction.py
│   │   ├── dependency_converter.py
│   │   ├── graph_building.py
│   │   ├── graph_model.py
│   │   ├── __init__.py
│   │   ├── LSTM_dependency_extractor.py
│   │   └── processing.py
├── README.md
└── sys_files
    └── db.info
```

## PROJECT FILES DESCRIPTION

`from_plain` - train, dev, test parts of dataset generated from simple sentences  
`from_syntagrus` - train, dev, test parts of dataset generated from SynTagRus  
`kapitanskaya_dochka.txt plain.txt nature.txt foreigners.txt read.txt` - texts for system evaluation  
`create_LSTM.ipynb` - notebook to train model for BiLSTM parser  
`BiLSTMParser.h5` - trained model for BiLSTM parser  
`fasttext` - serialized and dumped official fasText model  
`example.py` - example code with developed API  
`test.py` - code for system evaluation  
`dataset_generator.py` - code for generating dataset based on SynTagRus  
`API.py` -  API implementation  
`conllu_dependency_extractor.py` - StanfordNLP based parser  
`constants.py` - constants and default paths  
`db_connector.py` - database connection utils (change default database, clear default database)  
`db_extraction.py` - processed data extractor  
`dependency_converter.py` - output formats converter (CONLLU)  
`graph_building.py` - graph builder to load processed data to database  
`graph_model.py` - graph description  
`LSTM_dependency_extractor.py` - BiLSTM based parser  
`processing.py` - processor based on one of the two implemented parsers  
`db.info` - info about names and sizes of created databases