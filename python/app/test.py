from python.libs.processing import Processor
from neomodel import config, clear_neo4j_database, db
from python.libs.constants import connection_URL
import rusenttokenize
from python.libs.db_extraction import TextExtractor
from python.libs.LSTM_dependency_extractor import BiLSTMParser
import numpy as np
import re
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from python.libs.constants import DEFAULT_STANFORDNLP_DIR, DEFAUL_BILSTM_MODEL, DEFAULT_FASTTEXT_MODEL


def eval_text(filename, processor, tokenizer, extractor, fasttext):
    with open('../../dataset/' + filename, 'r') as f:
        source_text = f.read()
    print(f'Text processed. Next sent_id in db: {processor.process_text(source_text)}')
    text = extractor.extract_all()

    sentences = rusenttokenize.ru_sent_tokenize(source_text)
    true_sent_len = []
    for sent in sentences:
        true_sent_len.append([len(tokenizer.findall(sent))])
    true_sent_len = np.array(true_sent_len)
    print(f'Shape of source sentences length matrix: {true_sent_len.shape}')

    vectorizer = TfidfVectorizer()
    vectorizer.fit(sentences)
    vocab = vectorizer.get_feature_names()
    true_tfidf = vectorizer.transform(sentences)
    print(f'Shape of tfidf matrix for source sentences: {true_tfidf.shape}')

    Y = list()
    for word in vocab:
        try:
            vec = fasttext[word]
        except KeyError:
            vec = np.ones(300).tolist()
        Y.append(vec)
    Y = np.array(Y)
    print(f'Shape of word embedding matrix: {Y.shape}')

    from_db_sent_len = []
    for sent in text:
        from_db_sent_len.append([len(tokenizer.findall(sent))])
    from_db_sent_len = np.array(from_db_sent_len)

    print(f'Shape of extracted sentences length matrix: {from_db_sent_len.shape}')

    from_db_tfidf = vectorizer.transform(text)
    print(f'Shape of tfidf matrix for extracted sentences: {from_db_tfidf.shape}')

    np.place(from_db_sent_len, from_db_sent_len == 0, [1])
    np.place(true_sent_len, true_sent_len == 0, [1])

    true_sent_vectors = np.matmul(true_tfidf.todense(), Y) / true_sent_len
    print(f'Shape of source text vector representation matrix: {true_sent_vectors.shape}')
    from_db_sent_vectors = np.matmul(from_db_tfidf.todense(), Y) / from_db_sent_len
    print(f'Shape of extracted text vector representation matrix: {from_db_sent_vectors.shape}')

    print(f'Mean cosine similarity {filename}: '
          f'{np.mean(cosine_similarity(true_sent_vectors, from_db_sent_vectors).diagonal())}')


if __name__ == '__main__':
    config.DATABASE_URL = connection_URL
    config.ENCRYPTED_CONNECTION = False

    processor = Processor(bilstm_path=DEFAUL_BILSTM_MODEL, fasttext_path=DEFAULT_FASTTEXT_MODEL,
                          stanfordnlp_dir=DEFAULT_STANFORDNLP_DIR, parser='bilstm')
    tokenizer = re.compile(f'[\\w]+')
    extractor = TextExtractor()
    fasttext = processor.get_fasttext()
    fasttext = fasttext if fasttext else BiLSTMParser.load_fasttext(DEFAULT_FASTTEXT_MODEL)

    filenames = ['plain.txt', 'read.txt', 'foreigners.txt', 'nature.txt', 'kapitanskaya_dochka.txt']
    for name in filenames:
        clear_neo4j_database(db)
        processor.set_start_id(0)
        eval_text(name, processor, tokenizer, extractor, fasttext)
