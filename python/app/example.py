from python.libs.API import API
from python.libs.constants import connection_URL
from memory_profiler import profile


@profile
def main():
    api = API(parser='bilstm', url=connection_URL)
    with open('../../dataset/plain.txt', 'r') as f:
        source_text = f.read()
        api.clear_db()
        api.process(source_text)


if __name__ == '__main__':
    main()
