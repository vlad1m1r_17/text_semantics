from python.libs.conllu_dependency_extractor import ConlluParser
from python.libs.dependency_converter import DependencyConverter
import conllu
from python.libs.constants import *
import stanfordnlp as nlp
import math
import numpy as np

OBJECT_PROPERTY = 'obj_prop'
OBJECT = 'obj'
SEPARATOR = '|'
VERB = 'verb'
VERB_PROPERTY = 'verb_prop'
SUBJECT = 'subj'
SUBJECT_PROPERTY = 'subj_prop'
UNKNOWN = 'UNK'


def process(sentence):
    processed = set()
    subject, processed = ConlluParser.get_subject(sentence, processed)
    subject_properties, processed = ConlluParser.get_noun_properties(sentence, subject, processed)
    verb, processed = ConlluParser.get_predicate(sentence.to_tree(), processed)
    verb_properties, processed = ConlluParser.get_verb_properties(sentence, verb, processed)
    obj, processed = ConlluParser.get_object(sentence, verb, processed)
    obl, processed = ConlluParser.get_obl(sentence, verb, processed)
    obj_properties, processed = ConlluParser.get_noun_properties(sentence, obj, processed)
    obl_properties, processed = ConlluParser.get_noun_properties(sentence, obl, processed)
    subject = subject if subject else []
    subject_properties = subject_properties if subject_properties else []
    verb = verb if verb else []
    verb_properties = verb_properties if verb_properties else []
    obj = obj if obj else []
    obl = obl if obl else []
    obj_properties = obj_properties if obj_properties else []
    obl_properties = obl_properties if obl_properties else []
    sent = []
    tag = []
    for token in sentence:
        if token['deprel'] == 'punct':
            continue
        sent.append(token['form'])
        if token in subject:
            tag.append(SUBJECT)
        elif token in subject_properties:
            tag.append(SUBJECT_PROPERTY)
        elif token in verb:
            tag.append(VERB)
        elif token in verb_properties:
            tag.append(VERB_PROPERTY)
        elif token in obj:
            tag.append(OBJECT)
        elif token in obl:
            tag.append(OBJECT)
        elif token in obj_properties:
            tag.append(OBJECT_PROPERTY)
        elif token in obl_properties:
            tag.append(OBJECT_PROPERTY)
        else:
            tag.append(UNKNOWN)
    sentence_str = ' '.join([w for w in sent])
    tags_str = '|'.join([w for w in tag])
    return sentence_str + '|' + tags_str + '\n'


def process_file(file):
    data = []
    with open(file) as f:
        parsed = conllu.parse_incr(f)
        for i, sent in enumerate(parsed):
            data.append(process(sent))
    return data


def process_plain_text_file(file):
    data = []
    converter = DependencyConverter()
    with open(file, 'r') as f:
        text = f.read()
    processor = nlp.Pipeline(lang='ru', use_gpu=False)
    res = processor(text)
    for sent in res.sentences:
        converted = converter(sent)
        data.append(process(converted))
    return data

def process_plain():
    file = '../../dataset/plain.txt'
    data = process_plain_text_file(file)
    print(len(data))
    folder = '../../dataset/from_plain/'
    base = math.floor(len(data) * 0.8)
    splits = [base, base + math.floor(0.5 * (len(data) - base)), len(data) - 1]
    splitted = np.split(np.array(data), splits)
    with open(folder + 'train.txt', 'w') as f:
        f.writelines(splitted[0])
    with open(folder + 'dev.txt', 'w') as f:
        f.writelines(splitted[1])
    with open(folder + 'test.txt', 'w') as f:
        f.writelines(splitted[2])


def process_syntagrus():
    folder = '../../dataset/from_syntagrus/'
    data = process_file(file_train)
    with open(folder + 'train.txt', 'w') as f:
        f.writelines(data)
    data = process_file(file_dev)
    with open(folder + 'dev.txt', 'w') as f:
        f.writelines(data)
    data = process_file(file_test)
    with open(folder + 'test.txt', 'w') as f:
        f.writelines(data)


if __name__ == '__main__':
    target = 'plain'
    if target == 'plain':
        process_plain()
    else:
        process_syntagrus()
