from python.libs.graph_model import SubjectNode, PredicateNode
import json
import time
from termcolor import colored


class TextExtractor:
    def __init__(self):
        pass

    @staticmethod
    def get_by_id(dictionary, id):
        try:
            retval = dictionary[str(id)]
        except KeyError:
            retval = ''
        return retval

    @staticmethod
    def extract_all():
        print('Extracting from DB')
        start = time.time()
        text = dict()
        subjects = SubjectNode.nodes
        processed = set()

        for s in subjects:
            o_arr = s.action.all()
            subject_main_words = json.loads(s.main_words) if s.main_words else dict()
            subject_properties = json.loads(s.subject_properties) if s.subject_properties else dict()
            for o in o_arr:
                verbs = s.action.all_relationships(o)
                object_properties = json.loads(o.object_properties) if o.object_properties else dict()
                for verb in verbs:
                    id = verb.sent_id
                    if id in processed:
                        continue
                    processed.add(id)
                    subj = TextExtractor.get_by_id(subject_main_words, id)
                    subj_props = TextExtractor.get_by_id(subject_properties, id)
                    predicate = verb.verb_word
                    verb_properties = verb.verb_properties
                    obj = verb.object_word
                    obj_props = TextExtractor.get_by_id(object_properties, id)
                    sent = ' '.join([subj_props, subj, verb_properties, predicate, obj_props, obj])
                    text[id] = sent

        nodes = PredicateNode.nodes
        node = nodes[0] if nodes else None
        if node:
            for o in node.action:
                object_properties = json.loads(o.object_properties) if o.object_properties else dict()
                arcs = node.action.all_relationships(o)
                for arc in arcs:
                    id = arc.sent_id
                    obj_props = TextExtractor.get_by_id(object_properties, id)
                    sent = ' '.join([arc.verb_properties, arc.verb_word, obj_props, arc.object_word])
                    text[id] = sent
            for c in node.only_action:
                arcs = node.only_action.all_relationships(c)
                for arc in arcs:
                    id = arc.sent_id
                    sent = ' '.join([arc.verb_properties, arc.verb_word])
                    text[id] = sent
        result = [v for i, v in sorted(text.items(), key=lambda item: item[0])]
        print(colored(f'Extracted: {time.time() - start}s', "green"))
        return result
