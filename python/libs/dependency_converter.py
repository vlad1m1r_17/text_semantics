import conllu
from collections import OrderedDict


class DependencyConverter:
    def __init__(self):
        pass

    def feats_to_dict(self, feats):
        f_list = feats.split('|')
        features = OrderedDict()
        for feat in f_list:
            if '=' in feat:
                key, value = feat.split('=')
                features[key] = value
        return features if features else None

    def __call__(self, sentence):
        retval = []
        for t in sentence.tokens:
            val = OrderedDict()
            val['id'] = int(t.words[0].index) if t.words[0].index != '_' else None
            val['form'] = t.words[0].text if t.words[0].text != '_' else None
            val['lemma'] = t.words[0].lemma if t.words[0].lemma != '_' else None
            val['upostag'] = t.words[0].upos if t.words[0].upos != '_' else None
            val['xpostag'] = t.words[0].xpos if t.words[0].xpos != '_' else None
            val['feats'] = self.feats_to_dict(t.words[0].feats)
            val['head'] = t.words[0].governor if t.words[0].governor != '_' else None
            val['deprel'] = t.words[0].dependency_relation if t.words[0].dependency_relation != '_' else None
            val['deps'] = [(val['deprel'], val['head'])]
            val['misc'] = None
            retval.append(val)
        return conllu.TokenList(retval)
