import keras
import pickle
import numpy as np
from keras.preprocessing.sequence import pad_sequences
import tensorflow as tf
import rusenttokenize
import stanfordnlp
from .constants import *
import time
from termcolor import colored
import re
from memory_profiler import profile


class BiLSTMParser:
    @profile
    def __init__(self, bilstm_model_path, stanfordnlp_dir, fasttext_model):
        print("Creating BILSTM parser")
        start = time.time()
        config = tf.compat.v1.ConfigProto()
        config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
        config.log_device_placement = True  # to log device placement (on which device the operation run)
        sess = tf.compat.v1.Session(config=config)
        tf.compat.v1.keras.backend.set_session(sess)
        print('Loading BILSTM model')
        s1 = time.time()
        self.model = keras.models.load_model(bilstm_model_path)
        print(colored(f"BILSTM model loaded: {time.time() - s1}s", 'green'))
        print('Loading STANFORDNLP POS_tagger, lemma')
        s1 = time.time()
        self.pos_tagger = stanfordnlp.Pipeline(processors='tokenize,pos,lemma', lang='ru', tokenize_pretokenized=True,
                                               models_dir=stanfordnlp_dir)
        print(colored(f"STANFORDNLP POS_tagger, lemma loaded: {time.time() - s1}s", 'green'))
        print('Loading FASTTEXT model')
        s1 = time.time()
        self.word_encoder = BiLSTMParser.load_fasttext(fasttext_model)
        print(colored(f"FASTTEXT loaded: {time.time() - s1}s", 'green'))

        pos = ['NOUN', 'ADJF', 'ADJS', 'COMP', 'VERB', 'INFN', 'PRTF', 'PRTS', 'GRND', 'NUMR',
               'ADVB', 'NPRO', 'PRED', 'PREP', 'CONJ', 'PRCL', 'INTJ', UNKNOWN]
        self.pos_encoder = dict()
        for i, t in enumerate(pos):
            val = np.zeros(len(pos))
            val[i] = 1
            self.pos_encoder[t] = val

        targets = [SUBJECT, SUBJECT_PROPERTY, VERB, VERB_PROPERTY, OBJECT, OBJECT_PROPERTY, UNKNOWN]
        size = len(targets)
        target_mappening = dict()
        for i, t in enumerate(targets):
            val = np.zeros(size)
            val[i] = 1
            target_mappening[t] = val
        self.decoder = dict((tuple(v), k) for k, v in target_mappening.items())
        self.tokenizer = re.compile(f'[\\w]+')
        print(colored(f"BILSTM parser created: {time.time() - start}s", 'green'))

    @staticmethod
    def load_fasttext(path):
        with open(path, 'rb') as f:
            return pickle.load(f)

    def process_text(self, text):
        print('Processing text with BILSTM')
        start = time.time()
        sentences = [self.tokenizer.findall(s.strip())
                     for s in rusenttokenize.ru_sent_tokenize(text)]
        sizes = [len(s) for s in sentences]
        pos_tags = self.pos_tagger(sentences)
        tags = []
        features = []
        for sentence in pos_tags.sentences:
            tags.append([token.words[0].upos for token in sentence.tokens])
            features.append([token.words[0] for token in sentence.tokens])
        x_padded = pad_sequences(sentences, padding='post', truncating='post', maxlen=max_len, dtype=object,
                                 value=UNKNOWN)
        tags = pad_sequences(tags, padding='post', truncating='post', maxlen=max_len, dtype=object, value=UNKNOWN)
        x_preprocessed = []
        for x, tag in zip(x_padded, tags):
            x = [self.word_vector(w, t) for w, t in zip(x, tag)]
            x_preprocessed.append(x)
        x_preprocessed = np.array(x_preprocessed)
        result = self.model.predict(x_preprocessed, batch_size=128, verbose=1)
        result = result.argmax(axis=2)
        res = list()
        for i, sent in enumerate(result):
            l = sizes[i]
            if l < max_len:
                res.append(sent[:l])
            else:
                res.append(sent)
        res = self.decode_output(res)
        output = []
        print(len(sentences), len(features), len(res))
        for sent, feats, output_tags in zip(sentences, features, res):
            output.append([{'word': w, 'normal_form': f.lemma, 'features': f.feats, 'tag': t} for
                           w, f, t in zip(sent, feats, output_tags)])
        print(colored(f'Text processing with BILSTM done: {time.time() - start}s', 'green'))
        return output

    def word_vector(self, word, pos_tag):
        if word == UNKNOWN:
            return np.zeros(300 + len(self.pos_encoder))
        try:
            embedding = self.word_encoder[word]
        except KeyError:
            embedding = np.zeros(300)
        try:
            tag = self.pos_encoder[pos_tag]
        except KeyError:
            tag = self.pos_encoder[UNKNOWN]
        embedding = np.concatenate((embedding, tag))
        return embedding

    def decode_output(self, output):
        decoded = []
        decoder = [v for k, v in self.decoder.items()]
        for sample in output:
            d = [decoder[w] for w in sample]
            decoded.append(d)
        return decoded

    @staticmethod
    def generate_output_for_phrase(phrase):
        def make_dict(token):
            res = dict()
            res['word'] = token['word']
            res['normal_form'] = token['normal_form']
            res['feats'] = token['features']
            return res
        return [make_dict(w) for w in phrase] if phrase else None

    def __call__(self, text):
        output = list()
        sentences = self.process_text(text)
        for sentence in sentences:
            subject = list()
            sub_props = list()
            predicate = list()
            verb_props = list()
            obj = list()
            obj_props = list()
            obl = list()
            obl_props = list()
            for token in sentence:
                t = token['tag']
                if t == SUBJECT:
                    subject.append(token)
                elif t == SUBJECT_PROPERTY:
                    sub_props.append(token)
                elif t == VERB:
                    predicate.append(token)
                elif t == VERB_PROPERTY:
                    verb_props.append(token)
                elif t == OBJECT:
                    obj.append(token)
                elif t == OBJECT_PROPERTY:
                    obj_props.append(token)
            keys = ['subject', 'predicate', 'obj', 'obl', 'sub_props', 'obj_props', 'obl_props', 'verb_props']
            result = dict()
            for key in keys:
                result[key] = BiLSTMParser.generate_output_for_phrase(locals()[key])
            output.append(result)
        return output

    def get_fasttext(self):
        return self.word_encoder
