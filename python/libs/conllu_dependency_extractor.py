import conllu
import copy
from .dependency_converter import DependencyConverter
import stanfordnlp as nlp
import re
from string import punctuation
import rusenttokenize
import time
from termcolor import colored


class ConlluParser:
    def __init__(self, stanfordnlp_dir):
        print('Creating CONLLU parser')
        start = time.time()
        print("Loading STANFORDNLP models")
        s1 = time.time()
        self.processor = nlp.Pipeline(lang='ru', use_gpu=True, tokenize_pretokenized=True, models_dir=stanfordnlp_dir)
        print(colored(f'STANFORDNLP models loaded: {time.time() - s1}s', "green"))
        self.tokenizer = re.compile(f'[\\w]+|[{punctuation}]')
        print(colored(f'CONLLU parser created: {time.time() - start}s', 'green'))

    def __call__(self, text):
        print('Processing text with CONLLU')
        start = time.time()
        pretokenized = [self.tokenizer.findall(s.strip()) for s in rusenttokenize.ru_sent_tokenize(text)]
        res = self.processor(pretokenized)
        converter = DependencyConverter()
        result = [self.parse_conllu_sentence(converter(sent)) for sent in res.sentences]
        print(colored(f'Text processing with CONLLU done: {time.time() - start}s', 'green'))
        return result

    @staticmethod
    def parse_conllu_sentence(sentence):
        processed = set()
        result = dict()
        subject, processed = ConlluParser.get_subject(sentence, processed)
        predicate, processed = ConlluParser.get_predicate(sentence.to_tree(), processed)
        obj, processed = ConlluParser.get_object(sentence, predicate, processed)
        obl, processed = ConlluParser.get_obl(sentence, predicate, processed)
        sub_props, processed = ConlluParser.get_noun_properties(sentence, subject, processed)
        obj_props, processed = ConlluParser.get_noun_properties(sentence, obj, processed)
        obl_props, processed = ConlluParser.get_noun_properties(sentence, obl, processed)
        verb_props, processed = ConlluParser.get_verb_properties(sentence, predicate, processed)
        keys = ['subject', 'predicate', 'obj', 'obl', 'sub_props', 'obj_props', 'obl_props', 'verb_props']
        for key in keys:
            result[key] = ConlluParser.generate_output_for_phrase(locals()[key])
        return result

    @staticmethod
    def get_predicate(sentence, processed):
        predicate = None
        if sentence.token['upostag'] in {'VERB', 'ADJ', 'ADV'}:
            predicate = [sentence.token]
            for c in sentence.children:
                if c.token['deprel'] in {'xcomp', 'det', 'aux', 'fixed', 'cop', 'csubj', 'csubj:pass', 'aux:pass'}:
                    predicate.append(c.token)
        if sentence.token['upostag'] in {'NOUN', 'PRON', 'PROPN', 'ADJ', 'NUM'} and \
                any([tmp.token['deprel'] == 'nsubj' for tmp in sentence.children]):
            predicate = [sentence.token]
            for c in sentence.children:
                if c.token['deprel'] in {'cop', 'fixed', 'expl'}:
                    predicate.append(c.token)
        if predicate:
            for w in predicate:
                processed.add(w['id'])
        return conllu.TokenList(predicate) if predicate else predicate, processed

    @staticmethod
    def get_subject(sentence, processed):
        subject = [w for w in sentence.filter(deprel='nsubj')]
        if len(subject) == 0:
            subject = [w for w in sentence.filter(deprel='nsubj:pass')]
        if len(subject) == 0:

            root = sentence.filter(deprel='root')[0]
            if root['upostag'] in {'NOUN', 'ADJ', 'PRON', 'PROPN', 'NUM'}:
                subject = [root]
            else:
                subject = None
        subject_ids = set([w['id'] for w in subject]) if subject else None
        if subject_ids:
            for w in sentence:
                if w['head'] in subject_ids and w['deprel'] in {'flat'}:
                    subject.append(w)
        if subject:
            for w in subject:
                processed.add(w['id'])
        return subject, processed

    @staticmethod
    def get_object(sentence, predicate, processed):
        obj = []
        predicate_ids = set()
        if not predicate:
            return None, processed
        for item in predicate:
            predicate_ids.add(item['id'])
        for word in sentence:
            if word['head'] in predicate_ids and word['deprel'] in {'obj', 'iobj'}:
                obj.append(word)
        if len(obj):
            for w in obj:
                processed.add(w['id'])
        return obj if len(obj) else None, processed

    @staticmethod
    def get_obl(sentence, predicate, processed):
        obl = []
        predicate_ids = set()
        if not predicate:
            return None, processed
        for item in predicate:
            predicate_ids.add(item['id'])
        for word in sentence:
            if word['head'] in predicate_ids and word['deprel'] in {'obl'} and word['upostag'] in \
                    {'NOUN', 'PRON', 'PROPN', 'ADJ'}:
                obl.append(word)
        if len(obl):
            for w in obl:
                processed.add(w['id'])
        return obl if len(obl) else None, processed

    @staticmethod
    def get_noun_properties(sentence, noun, processed):
        if not noun:
            return None, processed
        properties = []
        noun_ids = [w['id'] for w in noun]
        for word in sentence:
            if word['id'] not in processed and word['head'] in noun_ids and word['deprel'] in \
                    {'nmod', 'nummod', 'appos', 'amod', 'det', 'case'}:
                properties.append(word)
                processed.add(word['id'])
        queue = copy.deepcopy(properties)
        while queue:
            element = queue.pop()
            dep = sentence.filter(head=element['id'])
            for el in dep:
                if el['deprel'] in {'nmod', 'nummod', 'appos', 'amod', 'det', 'case', 'compound', 'conj'} \
                        and el['id'] not in processed:
                    properties.append(el)
                    processed.add(el['id'])
                    queue.append(el)
        return properties, processed

    @staticmethod
    def get_verb_properties(sentence, verb, processed):
        if not verb:
            return None, processed
        properties = []
        verb_ids = [w['id'] for w in verb]
        for word in sentence:
            if word['id'] not in processed and word['head'] in verb_ids and \
                    (word['deprel'] == 'advmod' or (word['deprel'] == 'obl' and word['upostag'] in {'ADJ', 'ADV'})):
                properties.append(word)
                processed.add(word['id'])
        queue = copy.deepcopy(properties)
        while queue:
            element = queue.pop()
            dep = sentence.filter(head=element['id'])
            for el in dep:
                if el['deprel'] == 'advmod' or (el['deprel'] == 'obl' and el['upostag'] in {'ADJ', 'ADV'}) \
                        or el['deprel'] == 'conj' and el['id'] not in processed:
                    properties.append(el)
                    processed.add(el['id'])
                    queue.append(el)
        return properties, processed

    @staticmethod
    def generate_output_for_phrase(phrase):
        def make_dict(token):
            res = dict()
            res['word'] = token['form']
            res['normal_form'] = token['lemma']
            res['feats'] = token['feats']
            return res
        return [make_dict(w) for w in phrase] if phrase else None
