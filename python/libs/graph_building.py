import json
from .graph_model import SubjectNode, PredicateNode
from .constants import FICTIVE_NODE


class GraphBuilder:
    def __init__(self, start_sent_id=0):
        self._id = start_sent_id

    def set_start_id(self, id):
        self._id = id

    def __node(self, main, properties=None, predicate=False, word=None):
        cls = PredicateNode if predicate else SubjectNode
        try:
            s = cls.nodes.get(main_normal_form=main)
        except cls.DoesNotExist:
            s = cls(main_normal_form=main)
        if predicate:
            s.save()
            return s
        if properties is not None:
            props = s.subject_properties
            props = json.loads(props) if props else dict()
            props[str(self._id)] = properties
            props = json.dumps(props, ensure_ascii=False)
            s.subject_properties = props
        if word:
            words = s.main_words
            words = json.loads(words) if words else dict()
            words[str(self._id)] = word
            words = json.dumps(words, ensure_ascii=False)
            s.main_words = words
        s.save()
        return s

    def set_object_property(self, node, properties):
        props = node.object_properties
        props = json.loads(props) if props else dict()
        props[str(self._id)] = properties
        props = json.dumps(props, ensure_ascii=False)
        node.object_properties = props
        node.save()

    @staticmethod
    def as_string(value, normal_form=False):
        key = 'normal_form' if normal_form else 'word'
        return ' '.join([s[key] for s in value]) if value else ''

    def sentence_to_graph(self, parsed_sentence):
        s, o, v = None, None, None

        subject_string = self.as_string(parsed_sentence['subject'], normal_form=True)
        subject_word = self.as_string(parsed_sentence['subject'])
        if subject_string:
            subj_props_string = self.as_string(parsed_sentence['sub_props'])
            s = self.__node(subject_string, subj_props_string, word=subject_word)
        verb_string = self.as_string(parsed_sentence['predicate'])
        verb_props_string = self.as_string(parsed_sentence['verb_props'])
        obj_string = self.as_string(parsed_sentence['obj'], normal_form=True)
        obl_string = self.as_string(parsed_sentence['obl'], normal_form=True)
        obj_string_rel = self.as_string(parsed_sentence['obj'])
        obl_string_rel = self.as_string(parsed_sentence['obl'])
        obj_props_string = self.as_string(parsed_sentence['obj_props'])
        if obl_string and not obj_string:
            obj_string_rel = obl_string_rel
            obj_string = obl_string
            obj_props_string = self.as_string(parsed_sentence['obl_props'])
        if obj_string:
            o = self.__node(obj_string)
            self.set_object_property(o, obj_props_string)
        if s and o:
            s.action.connect(o, {'verb_word': verb_string, 'verb_properties': verb_props_string,
                                 'object_word': obj_string_rel, 'sent_id': self._id})
        elif s and not o:
            s.action.connect(s, {'verb_word': verb_string, 'verb_properties': verb_props_string,
                                 'object_word': '', 'sent_id': self._id})
        elif not s:
            v = self.__node(FICTIVE_NODE, None, predicate=True)
            if o:
                v.action.connect(o, {'verb_word': verb_string, 'verb_properties': verb_props_string,
                                     'object_word': obj_string_rel, 'sent_id': self._id})
            else:
                v.only_action.connect(v, {'verb_word': verb_string, 'verb_properties': verb_props_string,
                                          'object_word': '', 'sent_id': self._id})
        self._id += 1

    def get_id(self):
        return self._id
