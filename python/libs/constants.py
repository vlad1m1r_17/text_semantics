import os
from pathlib import Path

HOME_DIR = str(Path.home())
file_train = os.path.join(HOME_DIR, "text_semantics/ru_syntagrus-ud-train.conllu")
file_dev = os.path.join(HOME_DIR, "text_semantics/ru_syntagrus-ud-dev.conllu")
file_test = os.path.join(HOME_DIR, "text_semantics/ru_syntagrus-ud-test.conllu")
connection_URL = 'bolt://neo4j:password@localhost:7687'
FICTIVE_NODE = 'FICTIVE'
OBJECT_PROPERTY = 'obj_prop'
OBJECT = 'obj'
VERB = 'verb'
VERB_PROPERTY = 'verb_prop'
SUBJECT = 'subj'
SUBJECT_PROPERTY = 'subj_prop'
UNKNOWN = 'UNK'
max_len = 40
DEFAULT_FASTTEXT_MODEL = os.path.join(HOME_DIR, 'text_semantics/models/fasttext')
DEFAULT_STANFORDNLP_DIR = os.path.join(HOME_DIR, 'stanfordnlp_resources')
DEFAUL_BILSTM_MODEL = os.path.join(HOME_DIR, 'text_semantics/models/BiLSTMParser.h5')
neo4j_config = '/etc/neo4j/neo4j.conf'
db_info = os.path.join(HOME_DIR, 'text_semantics/sys_files/db.info')
neo4j_log = '/var/log/neo4j/neo4j.log'
