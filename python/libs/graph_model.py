import neomodel


class RelationshipModel(neomodel.StructuredRel):
    verb_word = neomodel.StringProperty(required=True)
    verb_properties = neomodel.StringProperty(required=False, default='')
    object_word = neomodel.StringProperty(required=False, default='')
    sent_id = neomodel.IntegerProperty(required=True)


class SubjectNode(neomodel.StructuredNode):
    main_normal_form = neomodel.StringProperty(required=True)
    main_words = neomodel.JSONProperty(required=False, default=None)
    subject_properties = neomodel.JSONProperty(required=False, default=None)
    object_properties = neomodel.JSONProperty(required=False, default=None)
    action = neomodel.RelationshipTo('SubjectNode', 'action', model=RelationshipModel)


class PredicateNode(neomodel.StructuredNode):
    main_normal_form = neomodel.StringProperty(required=True)
    action = neomodel.RelationshipTo('SubjectNode', 'action', model=RelationshipModel)
    only_action = neomodel.RelationshipTo('PredicateNode', 'action', model=RelationshipModel)

