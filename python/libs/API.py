from .constants import DEFAULT_STANFORDNLP_DIR, DEFAUL_BILSTM_MODEL, DEFAULT_FASTTEXT_MODEL, connection_URL
from .db_connector import DBConnector
from .processing import Processor


class API():
    def __init__(self, parser='bilstm', bilstm_path=DEFAUL_BILSTM_MODEL,
                 stanfordnlp_dir=DEFAULT_STANFORDNLP_DIR, fasttext_path=DEFAULT_FASTTEXT_MODEL, url=connection_URL):
        self.processor = Processor(bilstm_path, stanfordnlp_dir, fasttext_path, parser, 0)
        self.connector = DBConnector(url)
        self.select_db()

    def select_db(self):
        start = self.connector.select_db()
        self.processor.set_start_id(start)

    def clear_db(self):
        self.processor.set_start_id(self.connector.clear_db())

    def process(self, text):
        l = self.processor.process_text(text)
        self.connector.update_info(l)
        self.connector.save_info()
