from .graph_building import GraphBuilder
from .conllu_dependency_extractor import ConlluParser
from .LSTM_dependency_extractor import BiLSTMParser
import time
from termcolor import colored


class Processor:
    def __init__(self, bilstm_path, stanfordnlp_dir, fasttext_path, parser='bilstm', start_id=0):
        if parser == 'bilstm':
            self.parser = BiLSTMParser(bilstm_model_path=bilstm_path, stanfordnlp_dir=stanfordnlp_dir,
                                       fasttext_model=fasttext_path)
            self.fasttext = self.parser.get_fasttext()
        elif parser == 'stanfordnlp':
            self.parser = ConlluParser(stanfordnlp_dir=stanfordnlp_dir)
            self.fasttext = None
        elif parser is None:
            self.parser = None
        else:
            raise NameError
        self.builder = GraphBuilder(start_id)

    def process_text(self, text):
        parsed = self.parser(text)
        print('Loading do DB')
        start = time.time()
        for sentence in parsed:
            self.builder.sentence_to_graph(sentence)
        print(colored(f'Loaded to DB: {time.time() - start}s', "green"))
        return self.builder.get_id()

    def process_connlu_sentence(self, sentence):
        self.builder.sentence_to_graph(sentence)
        return self.builder.get_id()

    def set_start_id(self, id):
        self.builder.set_start_id(id)

    def get_fasttext(self):
        return self.fasttext
