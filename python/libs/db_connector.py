from python.libs.constants import db_info, neo4j_config, neo4j_log
import os
from neomodel import db, clear_neo4j_database, config


class DBConnector:
    def __init__(self, URL):
        config.DATABASE_URL = URL
        config.ENCRYPTED_CONNECTION = False
        with open(db_info, 'r') as f:
            data = f.readlines()
        self.dbs = dict()
        for line in data:
            name, l = line.split()
            self.dbs[name] = int(l)
        self.DB = None
        self.start_id = None

    def select_db(self):
        print('Available databases')
        for k in self.dbs:
            print(k, self.dbs[k])
        print('Enter DB name to work with:')
        self.DB = input()
        while len(self.DB) > 63 or len(self.DB) < 3:
            print("Invalid name")
            print('Enter DB name to work with:')
            self.DB = input()

        try:
            self.start_id = self.dbs[self.DB]
        except KeyError:
            self.start_id = 0

        if self.DB:
            with open(neo4j_config, 'r') as f:
                config = f.readlines()
            for i, line in enumerate(config):
                if 'dbms.default_database' in line:
                    txt, base = line.split('=')
                    print(f'DB before change: {base}')
                    if base.strip() == self.DB:
                        print('No DB change needed')
                        return self.start_id
                    config[i] = 'dbms.default_database='+self.DB + '\n'
                    break
            print('Enter sudo password:')
            sudo_password = input()
            command = 'neo4j stop'
            os.system('echo %s|sudo -S %s' % (sudo_password, command))
            with open(neo4j_config, 'w') as f:
                f.writelines(config)
            command = 'neo4j start'
            os.system('echo %s|sudo -S %s' % (sudo_password, command))
        else:
            raise ValueError
        ready = False
        while not ready:
            with open(neo4j_log, 'r') as f:
                last = f.readlines()[-1]
                if 'Remote interface available at' in last:
                    ready = True
        return self.start_id

    def clear_db(self):
        clear_neo4j_database(db)
        self.start_id = 0
        return self.start_id

    def update_info(self, l):
        self.dbs[self.DB] = l

    def save_info(self):
        with open(db_info, 'w') as f:
            for k in self.dbs:
                f.write(k + ' ' + str(self.dbs[k]) + '\n')